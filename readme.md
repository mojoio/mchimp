# mchimp
api abstraction package for mailchimp

## Availabililty
[![npm](https://mojoio.gitlab.io/assets/repo-button-npm.svg)](https://www.npmjs.com/package/mchimp)
[![git](https://mojoio.gitlab.io/assets/repo-button-git.svg)](https://GitLab.com/mojoio/mchimp)
[![git](https://mojoio.gitlab.io/assets/repo-button-mirror.svg)](https://github.com/mojoio/mchimp)
[![docs](https://mojoio.gitlab.io/assets/repo-button-docs.svg)](https://mojoio.gitlab.io/mchimp/)

## Status for master
[![build status](https://GitLab.com/mojoio/mchimp/badges/master/build.svg)](https://GitLab.com/mojoio/mchimp/commits/master)
[![coverage report](https://GitLab.com/mojoio/mchimp/badges/master/coverage.svg)](https://GitLab.com/mojoio/mchimp/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/mchimp.svg)](https://www.npmjs.com/package/mchimp)
[![Dependency Status](https://david-dm.org/mojoio/mchimp.svg)](https://david-dm.org/mojoio/mchimp)
[![bitHound Dependencies](https://www.bithound.io/github/mojoio/mchimp/badges/dependencies.svg)](https://www.bithound.io/github/mojoio/mchimp/master/dependencies/npm)
[![bitHound Code](https://www.bithound.io/github/mojoio/mchimp/badges/code.svg)](https://www.bithound.io/github/mojoio/mchimp)
[![TypeScript](https://img.shields.io/badge/TypeScript-2.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%206.x.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Usage
Use TypeScript for best in class instellisense.

For further information read the linked docs at the top of this README.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy.html)

[![repo-footer](https://mojoio.gitlab.io/assets/repo-footer.svg)](https://lossless.com)
